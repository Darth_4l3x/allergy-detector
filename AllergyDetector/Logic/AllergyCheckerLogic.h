//
//  AllergyCheckerLogic.h
//  AllergyDetector
//
//  Created by Alexander Rummel on 04.03.14.
//  Copyright (c) 2014 Arutech. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AllergyProfile.h"

typedef enum
{
    NoAllergies,
    AllergyFound,
    CodeCouldNotBeVerified,
    QrCodeIsNotInTheActualVersion
}AllergyLogicResult;

@interface AllergyCheckerLogic : NSObject
{
    BOOL _cancelOnVersionConflict;
    int _warningCount;
    NSMutableArray *_warnings;
}

-(id)initWithCancelOnVersionConflict:(BOOL) value;

- (AllergyLogicResult) CheckForAllergy:(NSString *)scanResult;

- (NSMutableArray *) GetWarnings;

- (int) GetWarningCount;

@end

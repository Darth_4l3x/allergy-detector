//
//  AllergyCheckerLogic.m
//  AllergyDetector
//
//  Created by Alexander Rummel on 04.03.14.
//  Copyright (c) 2014 Arutech. All rights reserved.
//

#define TEST

#import "AllergyCheckerLogic.h"

@implementation AllergyCheckerLogic

-(id)init
{
    self = [self initWithCancelOnVersionConflict:NO];
    if (!self) return nil;
    return self;
}

-(id)initWithCancelOnVersionConflict:(BOOL)value
{
    self = [super init];
    _cancelOnVersionConflict = value;
    _warningCount = 0;
    if (!self) return nil;
    return self;
}

- (NSMutableArray *)GetWarnings
{
    return _warnings;
}

- (int)GetWarningCount
{
    return _warningCount;
}

- (AllergyLogicResult)CheckForAllergy:(NSString *)scanResult
{
    
#ifdef TEST
    scanResult = @"BEGIN:FADF;VERSION:1.0;FA:001,002,003,004;END:FADF";
#endif
    NSArray *splitted = [scanResult componentsSeparatedByString:@";"];
    
    // Check the Format from the Barcode
    if(![splitted[0] isEqualToString:@"BEGIN:FADF"])
        return CodeCouldNotBeVerified;
    
    // Check the Version
    NSString *version = splitted[1];
    NSArray *versionArray = [version componentsSeparatedByString:@":"];
    NSString *versionNumber = versionArray[1];
    float actualVerison = [self GetActualVersion];
    float barcodeVersion = [versionNumber floatValue];
    
    if(actualVerison > barcodeVersion)
    {
        if(_cancelOnVersionConflict == YES)
        {
            return QrCodeIsNotInTheActualVersion;
        }
        else
        {
            _warnings = [[NSMutableArray alloc]init];
            _warningCount ++;
            [_warnings addObject:@"Der Barcode wurde nicht mit der neusten Allergen-Datenbank Version erstellt"];
        }
    }
    
    
    // Get the captured Allergies
    NSString *allergyPart = splitted[2];
    NSArray *splittedAllergyPart = [allergyPart componentsSeparatedByString:@":"];
    NSString *allergyString = splittedAllergyPart[1];
    NSArray *detectedAllergies = [allergyString componentsSeparatedByString:@","];
    
    NSMutableArray *profiles = [self GetAllergysForAllProfiles];
    
    bool containsAllergy = NO;
    
    // Check all the Profiles
    for(int i = 0; i < profiles.count; i++)
    {
        AllergyProfile *profile = [profiles objectAtIndex:i];
        NSMutableArray *allergies = [profile GetAllergies];
        
        for(int i = 0; i < allergies.count; i++)
        {
            NSString *allergy = allergies[i];
            
            if([detectedAllergies containsObject:allergy])
            {
                containsAllergy = YES;
            }
        }
    }
    
    if(containsAllergy == YES)
        return AllergyFound;
    else
        return NoAllergies;
}

-(NSMutableArray *) GetAllergysForAllProfiles
{
    NSMutableArray *result = [[NSMutableArray alloc]init];
    NSMutableArray *alg = [[NSMutableArray alloc] init];
    [alg addObject:@"001"];
    [alg addObject:@"002"];
    
    AllergyProfile *profile = [[AllergyProfile alloc] initWithProfileId:1 profileName:@"Test" allergies:alg];
    [result addObject:profile];
    return result;
}

-(float) GetActualVersion
{
    return 1.0;
}

@end

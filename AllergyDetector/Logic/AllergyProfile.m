//
//  AllergyItem.m
//  AllergyDetector
//
//  Created by Alexander Rummel on 04.03.14.
//  Copyright (c) 2014 Arutech. All rights reserved.
//

#import "AllergyProfile.h"

@implementation AllergyProfile

-(id)initWithProfileId:(int)profileId_ profileName:(NSString *)name allergies:(NSMutableArray *)allergyArray
{
    self = [super init];
    profileId = profileId;
    profileName = name;
    allergies = allergyArray;
    
    
    return self;
}

-(NSMutableArray *) GetAllergies
{
    return allergies;
}

@end

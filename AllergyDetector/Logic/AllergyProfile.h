//
//  AllergyItem.h
//  AllergyDetector
//
//  Created by Alexander Rummel on 04.03.14.
//  Copyright (c) 2014 Arutech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllergyProfile : NSObject
{
    int profileId;
    NSString *profileName;
    NSMutableArray *allergies;
}

-(id)initWithProfileId:(int)profileId_ profileName:(NSString *)name allergies:(NSMutableArray *) allergyArray;

-(NSMutableArray *) GetAllergies;

@end

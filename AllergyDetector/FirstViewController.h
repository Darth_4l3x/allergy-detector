//
//  FirstViewController.h
//  AllergyDetector
//
//  Created by Alexander Rummel on 02.03.14.
//  Copyright (c) 2014 Arutech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZXingWidgetController.h>
#import "AllergyCheckerLogic.h"

@interface FirstViewController : UIViewController <ZXingDelegate>
@property (strong, nonatomic) AllergyCheckerLogic *logic;
- (IBAction)btnStartScan_TouchUpInside:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *tvResultView;

@end

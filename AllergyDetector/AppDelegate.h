//
//  AppDelegate.h
//  AllergyDetector
//
//  Created by Alexander Rummel on 02.03.14.
//  Copyright (c) 2014 Arutech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  FirstViewController.m
//  AllergyDetector
//
//  Created by Alexander Rummel on 02.03.14.
//  Copyright (c) 2014 Arutech. All rights reserved.
//

#import "FirstViewController.h"
#import <MultiFormatReader.h>

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _logic = [[AllergyCheckerLogic alloc] init];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnStartScan_TouchUpInside:(id)sender
{
    ZXingWidgetController *widController = [[ZXingWidgetController alloc] initWithDelegate:self showCancel:YES OneDMode:NO];
    
    NSMutableSet *readers = [[NSMutableSet alloc] init];
    
    widController.readers = readers;
    
    MultiFormatReader *reader = [[MultiFormatReader alloc] init];
    [readers addObject:reader];
    
    //[self presentModalViewController:widController animated:YES];
    [self presentViewController:widController animated:YES completion:Nil];
}

- (void) zxingController:(ZXingWidgetController *)controller didScanResult:(NSString *)result
{
    if(self.isViewLoaded)
    {
        [self.tvResultView setText:result];
        AllergyLogicResult checkResult = [_logic CheckForAllergy:result];
        if(checkResult == CodeCouldNotBeVerified)
        {
            [self.tvResultView setText:@"Code konnte nicht gescannt werden"];
        }
        else if (checkResult == AllergyFound)
        {
            [self.tvResultView setText:@"Du solltest das nicht essen :-("];
        }
        else
        {
            [self.tvResultView setText:@"Du kannst das essen :-)"];
        }
        
    }
    
    [self dismissViewControllerAnimated:YES completion:Nil];
    //[self dismissModalViewControllerAnimated:NO];
}

- (void) zxingControllerDidCancel:(ZXingWidgetController *)controller
{
    [self dismissViewControllerAnimated:YES completion:Nil];
    //[self dismissModalViewControllerAnimated:NO];
}

@end
